package devhibernate.base.entity;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
@Entity
@Table(name = "persona")
public class Persona implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "edad")
	private int edad;

	@ManyToOne
	@JoinColumn(name = "pais")
	private Pais pais;

	@ManyToMany
	@JoinTable(name = "personas_aficiones", joinColumns = @JoinColumn(name = "persona_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "aficion_id", referencedColumnName = "id"))
	private Collection<Aficion> aficiones;

}
