package devhibernate.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import devhibernate.base.dao.HibernateUtils;
import devhibernate.base.entity.Aficion;
import devhibernate.base.entity.Pais;
import devhibernate.base.entity.Persona;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Transaction transaction = null;
		try (Session session = HibernateUtils.getSessionFactory().openSession()) {

			transaction = session.beginTransaction();

			/*
			 * Pais pais = new Pais(); pais.setNombre("Australia"); session.save(pais); Pais
			 * pais1 = new Pais(); pais1.setNombre("Espa�a"); session.save(pais1); Pais
			 * pais2 = new Pais(); pais2.setNombre("Francia"); session.save(pais2); Pais
			 * pais3 = new Pais(); pais3.setNombre("Italia"); session.save(pais3); Pais
			 * pais4 = new Pais();
			 * 
			 * pais4.setNombre("Protugal"); session.save(pais4);
			 * 
			 * Persona persona = new Persona(); persona.setNombre("chamit");
			 * persona.setEdad(38); persona.setPais(pais4);
			 * 
			 * session.save(persona);
			 */

			// transaction.commit();

			/*
			 * Query query = session.createQuery("select p from Pais as p");
			 * query.setFirstResult(1); query.setMaxResults(6);
			 * 
			 * List<Pais> list = query.getResultList();
			 * 
			 * for(Pais obj: list) { System.out.println(obj.getNombre()); }
			 * 
			 * Persona persona = (Persona) session.get(Persona.class, 1);
			 * 
			 * System.out.println(persona.getNombre());
			 * 
			 * Pais pais = (Pais) session.get(Pais.class, 57);
			 * 
			 * System.out.println(pais.getNombre());
			 * 
			 * Persona persona2 = new Persona(); persona2.setNombre("chamit");
			 * persona2.setEdad(38); persona2.setPais(pais);
			 * 
			 * session.save(persona2); transaction.commit();
			 */

			Persona persona = (Persona) session.get(Persona.class, 2);
			/*persona.getPais();

			Query query = session.createQuery("select p from Pais as p where id = :id");
			query.setParameter("id", 57);
			Pais pais = (Pais) query.getSingleResult();*/

			Aficion aficion = new Aficion();

			aficion.setNombre("Foot Ball");
			aficion.setCosteMensual(20);

			//session.save(aficion);
			int id = (int) session.save(aficion);
			
			Aficion aficion2 = (Aficion) session.get(Aficion.class, id);
			
			System.out.println(aficion2.getNombre());
			
			List<Aficion> aficiones = new ArrayList<>();

			aficiones.add(aficion2);

			persona.getAficiones().add(aficion);

			session.update(persona);

			transaction.commit();

		} catch (Exception e) {

			if (transaction != null) {
				transaction.rollback();
			}

			e.printStackTrace();
		}
	}

}
